from datetime import datetime, date, time, timedelta
from bs4 import BeautifulSoup
import requests
import json
import re

dia_semana = int(datetime.now().strftime('%w'))

angelus = """
V. El Ángel del Señor anunció a María.
R. Y concibió por obra del Espíritu Santo.
Dios te salve, María… Santa María…

V. He aquí la esclava del Señor.
R. Hágase en mí según tu palabra.
Dios te salve, María… Santa María…

V. Y el Verbo se hizo carne.
R. Y habitó entre nosotros.
Dios te salve, María… Santa María…

V. Ruega por nosotros, santa Madre de Dios.
R. Para que seamos dignos de alcanzar las promesas de Cristo.

Oremos:
Derrama, Señor, tu gracia sobre nosotros, que, por el anuncio del Ángel, hemos conocido la encarnación de tu Hijo, para que lleguemos, por su pasión y su cruz, a la gloria de la resurrección. Por Jesucristo, nuestro Señor.

R. Amén.

Gloria al Padre…. (3 veces)

"""

letanias = """
Letanías

Señor, ten piedad
Cristo, ten piedad
Señor, ten piedad.
Cristo, óyenos.
Cristo, escúchanos.

Dios, Padre celestial, 
ten piedad de nosotros.

Dios, Hijo, Redentor del mundo, 
Dios, Espíritu Santo, 
Santísima Trinidad, un solo Dios,

Santa María, 
ruega por nosotros.
Santa Madre de Dios,
Santa Virgen de las Vírgenes,
Madre de Cristo, 
Madre de la Iglesia, 
Madre de la misericordia, 
Madre de la divina gracia, 
Madre de la esperanza, 
Madre purísima, 
Madre castísima, 
Madre siempre virgen,
Madre inmaculada, 
Madre amable, 
Madre admirable, 
Madre del buen consejo, 
Madre del Creador, 
Madre del Salvador, 
Virgen prudentísima, 
Virgen digna de veneración, 
Virgen digna de alabanza, 
Virgen poderosa, 
Virgen clemente, 
Virgen fiel, 
Espejo de justicia, 
Trono de la sabiduría, 
Causa de nuestra alegría, 
Vaso espiritual, 
Vaso digno de honor, 
Vaso de insigne devoción, 
Rosa mística, 
Torre de David, 
Torre de marfil, 
Casa de oro, 
Arca de la Alianza, 
Puerta del cielo, 
Estrella de la mañana, 
Salud de los enfermos, 
Refugio de los pecadores, 
Consuelo de los migrantes,
Consoladora de los afligidos, 
Auxilio de los cristianos, 
Reina de los Ángeles, 
Reina de los Patriarcas, 
Reina de los Profetas, 
Reina de los Apóstoles, 
Reina de los Mártires, 
Reina de los Confesores, 
Reina de las Vírgenes, 
Reina de todos los Santos, 
Reina concebida sin pecado original, 
Reina asunta a los Cielos, 
Reina del Santísimo Rosario, 
Reina de la familia, 
Reina de la paz.

Cordero de Dios, que quitas el pecado del mundo, 
perdónanos, Señor.

Cordero de Dios, que quitas el pecado del mundo, 
escúchanos, Señor.

Cordero de Dios, que quitas el pecado del mundo, 
ten misericordia de nosotros.

Ruega por nosotros, Santa Madre de Dios. 
Para que seamos dignos de las promesas de Cristo.

ORACIÓN. 
Te rogamos nos concedas, 
Señor Dios nuestro, 
gozar de continua salud de alma y cuerpo, 
y por la gloriosa intercesión 
de la bienaventurada siempre Virgen María, 
vernos libres de las tristezas de la vida presente 
y disfrutar de las alegrías eternas. 
Por Cristo nuestro Señor. 
Amén. 
            """

misterios_gloriosos = [
    'Contemplamos los Misterios Gloriosos:\n',
    'La resurrección del Señor',
    'La ascensión del Señor',
    'El advenimiento del Espíritu Santo sobre María y los Apóstoles',
    'La asunción de María',
    'La coronación de la Virgen'
]

misterios_dolorosos = [
    'Contemplamos los Misterios Dolorosos:\n',
    'La oración del Señor en el huerto de Getsemaní',
    'La flagelación del Señor',
    'La coronación de espinas',
    'El tránsito del Señor con la cruz a cuestas',
    'La Crucifixión y muerte del Señor en el monte Calvario'
]

misterios_gozosos = [
    'Contemplamos los Misterios Gozosos:\n',
    'El anuncio del ángel a la Virgen María',
    'La visitación de María a su prima Isabel',
    'La Natividad del Señor',
    'La presentación del Señor',
    'Encuentro del Señor en el templo'
]

misterios_luminosos = [
    'Contemplamos los Misterios Luminosos:\n',
    'El bautismo de Jesús en el río Río Jordán',
    'La autorrevelación del Señor en las bodas de Caná',
    'La predicación del Reino de Dios y la Conversión',
    'La transfiguración del Señor en el Monte Tabor',
    'La institución de la Eucaristía en la Última Cena'
]


def devolver_rosario():
    if dia_semana == 3 or dia_semana == 0:
        return misterios_gloriosos
    elif dia_semana == 2 or dia_semana == 5:
        return misterios_dolorosos
    elif dia_semana == 1 or dia_semana == 6:
        return misterios_gozosos
    elif dia_semana == 4:
        return misterios_luminosos
    else:
        return ['Error: No se ha podido devolver el rosario, lo siento.']


def devolver_evangelio():
    formato2 = "%Y-%m-%d"
    hoy = datetime.today()
    cadena1 = hoy.strftime(formato2)
    # print(cadena1)

    URL = "https://publication.evangelizo.ws/SP/days/" + cadena1 + "?from=gospelComponent"

    # Realizamos la petición a la web
    req = requests.get(URL)

    # Comprobamos que la petición nos devuelve un Status Code = 200
    status_code = req.status_code
    if status_code == 200:

        # Pasamos el contenido HTML de la web a un objeto BeautifulSoup()
        # html = BeautifulSoup(req.text, "html.parser")
        data = json.loads(req.text)

        # print(data)
        for i in data['data']['readings']:
            if i['type'] == 'gospel':
                texto = i['book']['full_title']

                palabra = i['text']
                palabra = re.sub(r"\n?\[\[.*?]]", ' ', palabra)

                texto = texto + ".\n" + palabra + "\nPalabra del Señor."

                # print(texto)
                # print('_________________________________________________________')
                return texto
    # Obtenemos todos los divs donde están las entradas
    # entradas = html.find_all('div', {'class': 'evangeli_text'})
    #
    # # Recorremos todas las entradas para extraer el título, autor y fecha
    # for i, entrada in enumerate(entradas):
    #     # Con el método "getText()" no nos devuelve el HTML
    #     titulo = entrada.find('strong').getText()
    #     lectura = entrada.find('span').getText()
    #     # Sino llamamos al método "getText()" nos devuelve también el HTML
    #     #autor = entrada.find('span', {'class': 'autor'})
    #     #fecha = entrada.find('span', {'class': 'fecha'}).getText()
    #
    #     # Imprimo el Título, Autor y Fecha de las entradas
    #     #print (titulo)
    #     #print (lectura)
    #
    #     return titulo+"\n"+lectura+"\n Palabra del Señor.\n"

    else:
        return "ERROR!"


def insertar_id(id):
    f = open('ids.txt')
    ids = f.read().split(';')
    id_string = str(id)

    # print(ids)

    if not id_string in ids:
        f = open('ids.txt', 'a')
        f.write(f';{id_string}')

    f.close()


def silenciar(id):
    id = f"{id}"

    f = open('ids.txt')
    ids = f.read().split(';')

    ids.remove(f"{id}")

    texto = ""
    i = 0

    for id in ids:
        if i == 0:
            texto += f"{id}"
        else:
            texto += f";{id}"
        i = i + 1

    f = open('ids.txt', 'w')
    f.write(texto)

    f.close()


devolver_evangelio()
