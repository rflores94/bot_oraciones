from crontab import CronTab
from datetime import datetime

entry = CronTab('25 * * * *')
entry.next()
entry.next(datetime(2011, 7, 17, 11, 25))