#!/usr/bin/python

# This is a simple echo bot using the decorator mechanism.
# It echoes any incoming text messages.

import telebot
import json
from telebot import types
import almacenamiento
import palabras
import admin as admin
import re
import comentario

API_TOKEN = '1358794054:AAEzvv_0m9-6bG9t2hM59NlQwx6ySAwWWIc'
ID_ADMIN = 144149707

bot = telebot.TeleBot(API_TOKEN)

markup = types.ReplyKeyboardMarkup()
markupAdmin = types.ReplyKeyboardMarkup()
markup.add('/evangelio', '/comentarioEvangelio', '/rosario', '/angelus', '/palabras', '/notificar', '/silenciar')
markupAdmin.add('/evangelio', '/comentarioEvangelio', '/rosario', '/angelus', '/palabras', '/notificar', '/silenciar')

NUMERADOR = ['', 'Primer misterio: ', 'Segundo misterio: ', 'Tercer misterio: ', 'Cuarto misterio: ',
             'Quinto misterio: ']


def strip_tags(value):
    return re.sub(r'<[^>]*?>', ' ', value)


# Handle '/start' and '/help'
@bot.message_handler(commands=['help', 'start'])
def send_welcome(message):
    # almacenamiento.insertar_id(message.chat.id)
    bienvenida = f"Holaa {message.chat.first_name}! Bienvenido a Oraciones, un bot creado para aquellas personas que quieren rezar toooodos los días. Estamos implementando el bot, no tardaremos en tener preparadas las oraciones diarias!"
    if message.chat.id == ID_ADMIN:
        bot.send_message(message.chat.id, bienvenida, None, None, markupAdmin)
    else:
        bot.send_message(message.chat.id, bienvenida, None, None, markup)


# Santo rosario
@bot.message_handler(commands=['rosario'])
def send_rosario(message):
    mensaje_para_enviar = ""
    i = 0

    for misterio in almacenamiento.devolver_rosario():

        mensaje_para_enviar += NUMERADOR[i] + misterio + "\n"
        if i > 0:
            mensaje_para_enviar += 'Padre nuestro...\n'
            mensaje_para_enviar += '10 x Ave María...\n'
            mensaje_para_enviar += 'Gloria...\n\n'
        i += 1

    if i > 1:
        mensaje_para_enviar += "\n\n" + almacenamiento.letanias

    if message.chat.id == ID_ADMIN:
        bot.send_message(message.chat.id, mensaje_para_enviar, None, None, markupAdmin)
    else:
        bot.send_message(message.chat.id, mensaje_para_enviar, None, None, markup)


# Nos manda el rezo del Angelus
@bot.message_handler(commands=['angelus'])
def send_angelus(message):
    if message.chat.id == ID_ADMIN:
        bot.send_message(message.chat.id, almacenamiento.angelus, None, None, markupAdmin)
    else:
        bot.send_message(message.chat.id, almacenamiento.angelus, None, None, markup)


# Devolvemos el id del chat
@bot.message_handler(commands=['id', 'my_id'])
def send_id(message):
    my_id = message.chat.id
    bot.send_message(message.chat.id,
                     f"Tu id es: {my_id}")


# Devolvemos la palabra
def extract_arg(arg):
    return arg.split()[1:]


@bot.message_handler(commands=['palabra'])
def send_palabra(message):
    my_id = message.chat.id
    palabra = str(extract_arg(message.text))
    palabra = palabra.replace(u'[\'', u'')
    palabra = palabra.replace(u'\']', u'')

    code = palabras.ejecutar(palabra)
    if (code == "error"):
        bot.send_message(message.chat.id,
                         f"No existen lecturas para la palaba: {palabra}")
    else:
        # bot.send_message(message.chat.id,
        #                  f"Te mando archivo con las lecturas de la palaba: {palabra}")
        with open('Palabra.xlsx', 'rb') as document_file:
            bot.send_document(message.chat.id,
                              document_file)


# Devolvemos el evangelio del día
@bot.message_handler(commands=['evangelio'])
def send_evangelio(message):
    if message.chat.id == ID_ADMIN:
        bot.send_message(message.chat.id, almacenamiento.devolver_evangelio(), None, None, markupAdmin)
    else:
        bot.send_message(message.chat.id, almacenamiento.devolver_evangelio(), None, None, markup)


# Devolvemos el listado de las palabras
@bot.message_handler(commands=['palabras'])
def send_evangelio(message):
    with open('palabras.json') as file:
        palabras = json.load(file)
        msg = 'Palabras disponibles:\n'
        for palabra in palabras:
            msg += f'{palabra}, '
    if message.chat.id == ID_ADMIN:
        bot.send_message(message.chat.id, msg, None, None, markupAdmin)
    else:
        bot.send_message(message.chat.id, msg, None, None, markup)


# Mandar comentario del evangelio
@bot.message_handler(commands=['comentarioEvangelio'])
def send_comment(message):
    if message.chat.id == ID_ADMIN:
        bot.send_message(message.chat.id, comentario.devolver_comentario_evangelio(), None, None, markupAdmin)
    else:
        bot.send_message(message.chat.id, comentario.devolver_comentario_evangelio(), None, None, markup)



# Activar notificaciones
@bot.message_handler(commands=['notificar'])
def notificar(message):
    # almacenamiento.insertar_id(message.chat.id)
    my_id = message.chat.id

    with open('/var/lib/jenkins/workspace/executeBotOraciones/admin.py[+4]/usuarios.json') as file:
        usuarios = json.loads(file.read())
        usuarios[my_id] = my_id
        with open('/var/lib/jenkins/workspace/executeBotOraciones/admin.py[+4]/usuarios.json', 'w') as file:
            json.dump(usuarios, file, indent=4)
    if message.chat.id == ID_ADMIN:
        bot.send_message(message.chat.id, f"Notificaciones activadas, {message.chat.first_name}", None, None,
                         markupAdmin)
    else:
        bot.send_message(message.chat.id, f"Notificaciones activadas, {message.chat.first_name}", None, None, markup)


# Desactivar notificaciones
@bot.message_handler(commands=['silenciar'])
def silenciar(message):
    # almacenamiento.silenciar(message.chat.id)
    my_id = message.chat.id
    with open('/var/lib/jenkins/workspace/executeBotOraciones/admin.py[+4]/usuarios.json') as file:
        usuarios = json.loads(file.read())
        del usuarios[str(my_id)]
        with open('/var/lib/jenkins/workspace/executeBotOraciones/admin.py[+4]/usuarios.json', 'w') as file:
            json.dump(usuarios, file, indent=4)
    if message.chat.id == ID_ADMIN:
        bot.send_message(message.chat.id, f"Notificaciones desactivadas, {message.chat.first_name}", None, None,
                         markupAdmin)
    else:
        bot.send_message(message.chat.id, f"Notificaciones desactivadas, {message.chat.first_name}", None, None, markup)


# Handle all other messages with content_type 'text' (content_types defaults to ['text'])
@bot.message_handler(func=lambda message: True)
def echo_message(message):
    bot.send_message(message.chat.id, message.text)


bot.polling(none_stop=True)
# bot.idle()
