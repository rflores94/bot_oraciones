import datetime
import os
from bs4 import BeautifulSoup
import requests, re
import json
import openpyxl
from openpyxl.styles import Font, Color, Alignment, Border, Side, colors

libro = ""

palabra_seleccionada = "";

palabras = {}

bloques = {
    'primer_bloque': [],
    'segundo_bloque': [],
    'tercer_bloque': [],
    'evangelios': []
}

indices = {
    'primer_bloque': 'a',
    'segundo_bloque': 'c',
    'tercer_bloque': 'e',
    'evangelios': 'g'
}


def generarExcel(bloques):
    wb = openpyxl.Workbook()
    hoja = wb.active
    hoja.merge_cells('A1:H1')

    TITULO = Font(
        name='Calibri',
        size=18,
        bold=True,
        italic=False,
        vertAlign=None,
        underline='none',
        strike=False,
    )

    BLOQUES = Font(
        name='Calibri',
        size=12,
        bold=True,
        italic=False,
        vertAlign=None,
        underline='none',
        strike=False,
    )

    RESTO = Font(
        name='Calibri',
        size=12,
        bold=False,
        italic=False,
        vertAlign=None,
        underline='none',
        strike=False,
    )

    centrado = Alignment(horizontal="center")

    hoja.column_dimensions['a'].width = 20
    hoja.column_dimensions['b'].width = 20
    hoja.column_dimensions['c'].width = 20
    hoja.column_dimensions['d'].width = 20
    hoja.column_dimensions['e'].width = 20
    hoja.column_dimensions['f'].width = 20
    hoja.column_dimensions['g'].width = 20
    hoja.column_dimensions['h'].width = 20

    hoja["a1"].alignment = centrado
    hoja['a1'].font = TITULO
    global palabra_seleccionada
    hoja['a1'] = palabra_seleccionada

    hoja.merge_cells('a2:b2')
    hoja["a2"].alignment = centrado
    hoja['a2'].font = BLOQUES
    hoja['a2'] = 'Primer Bloque'

    hoja.merge_cells('c2:d2')
    hoja["c2"].alignment = centrado
    hoja['c2'].font = BLOQUES
    hoja['c2'] = 'Segundo Bloque'

    hoja.merge_cells('e2:f2')
    hoja["e2"].alignment = centrado
    hoja['e2'].font = BLOQUES
    hoja['e2'] = 'Tercer Bloque'

    hoja.merge_cells('g2:h2')
    hoja["g2"].alignment = centrado
    hoja['g2'].font = BLOQUES
    hoja['g2'] = 'Evangelios'

    # for i in range(10):
    #     num = str(i + 3)
    #     hoja['a' + num] = "Prueba"

    # for bloque in bloques:
    #     print(bloque)
    #     for libro in bloques[bloque]:
    #         print(libro)
    #     print()

    global indices
    for bloque in bloques:
        # print(bloque)
        i = 3
        for libro in bloques[bloque]:
            # print(libro)
            num = str(i)
            hoja[indices[bloque] + num].font = RESTO
            hoja[indices[bloque] + num] = libro
            i += 1

    # _titulo = hoja.cell(wb, 1, 1, 'Nombre de la palabra')
    # _titulo.style.font.name = 'Arial'
    # _titulo.style.font.size = 15
    # _titulo.style.font.bold = True
    # _titulo.style.alignament.wrap_text = True

    # _titulo = 'Titulo de la palabra'
    # # Crea la fila del encabezado con los titulos
    # hoja.append(('Nombre', 'Referencia', 'Stock', 'Precio'))
    # for producto in productos:
    #     # producto es una tupla con los valores de un producto
    #     hoja.append(producto)
    if os.path.isfile('Palabra.xlsx'):
        os.remove('Palabra.xlsx')
    wb.save('Palabra.xlsx')
    wb.close()


def mostrarBloques():
    # for bloque in bloques:
    #     print(bloque)
    #     for libro in bloques[bloque]:
    #         print(libro)
    #     print()
    generarExcel(bloques)


def comprobarBloques(cita):
    with open('bloques.json') as file:
        data = json.load(file)
        # print(data['primer_bloque'])
        for bloque in data:
            # print(bloque)
            # print("______________")
            for libro in data[bloque]:
                matchObj = re.match(r"^(\d?\D{2,})", cita)
                if matchObj:
                    libro_cita = matchObj.group(1)
                    if libro == libro_cita.strip():
                        global bloques
                        bloques[bloque].append(cita)
                # print(libro)
            # print()
            # print(libro)
    # mostrarBloques()


# Le pasamos la url de la palabra que vamos a escanear y nos saca las citas
def escanearPalabra(url):
    req = requests.get(url)

    # Comprobamos que la petici2n nos devuelve un Status Code = 200
    status_code = req.status_code
    if status_code == 200:
        # Pasamos el contenido HTML de la web a un objeto BeautifulSoup()
        html = BeautifulSoup(req.text, "html.parser")
        citas = html.find_all('cite')

        # print("\n")

        # Recorremos todas las citas
        for i, cita in enumerate(citas):
            name = cita.getText()
            # Comprobamos que tengan el libro
            matchObj = re.match(r"^(\d?\D{2,})", name)
            if matchObj:
                # Si tienen el libro, lo almacenamos en la variable global
                global libro
                libro = matchObj.group(1)
            else:
                # Si no tienen el libro, le concatenamos el libro anterior, almacenado en la variable local
                name = libro + name

            # print("CITA:: " + name)
            comprobarBloques(name)


def buscarJson(pet):
    global palabra_seleccionada
    palabra_seleccionada = pet.upper()
    global palabras
    if pet.lower() in palabras:
        escanearPalabra(palabras[pet.lower()])
        mostrarBloques()


def buscarWeb(pet):
    URL = "https://hjg.com.ar/vocbib/"
    # URL = "https://mercadao.pt/api/catalogues/5e8d7d0327783a0020f61e4d/with-descendants"

    # Realizamos la petici?n a la web
    req = requests.get(URL)

    # Comprobamos que la petici?n nos devuelve un Status Code = 200
    status_code = req.status_code
    if status_code == 200:

        # Pasamos el contenido HTML de la web a un objeto BeautifulSoup()
        html = BeautifulSoup(req.text, "html.parser")
        # site_json = json.loads(html.text)
        # print(site_json)

        # Obtenemos la tabla
        table = html.find('table')
        # Obtenemos todos los enlaces dentro de la tabla
        enlaces = table.find_all('a')

        # Recorremos todas los enlaces
        for i, enlace in enumerate(enlaces):
            # Con el m?todo "getText()" no nos devuelve el HTML
            palabra = enlace.getText()
            url = "https://hjg.com.ar/vocbib/" + enlace.get('href')
            # print("\nPALABRA:: " + palabra + "\tURL:: " + url + "\n")
            global palabras
            palabras[palabra.lower()] = url
            # escanearPalabra(url)
            # break
        with open('palabras.json', 'w') as file:
            json.dump(palabras, file, indent=4)
        # mostrarBloques()
        # print(palabras)
        # for palabra in palabras:
        # print(palabra.upper())
        # print("Dime una palabra: ", end="")
        # pet = input()
        global palabra_seleccionada
        palabra_seleccionada = pet.upper()
        if pet.lower() in palabras:
            escanearPalabra(palabras[pet.lower()])
            mostrarBloques()

        # else:
        # print("No existe esa palabra...")

    # else:
    # print("\nERROR!!\n")


def ejecutar(pet):
    global bloques
    bloques = {
        'primer_bloque': [],
        'segundo_bloque': [],
        'tercer_bloque': [],
        'evangelios': []
    }
    if os.path.isfile('palabras.json'):
        # global palabras
        with open('palabras.json') as f:
            global palabras
            palabras = json.load(f)
            buscarJson(pet)
    else:
        buscarWeb(pet)


# ejecutar('abraham')

# try:
#     with open('palabras.json', 'r') as f:
#         buscarJson('cruz')
# except FileNotFoundError as e:
#     ejecutar('cruz')
# except IOError as e:
#     ejecutar('cruz')
# ejecutar('cruz')
