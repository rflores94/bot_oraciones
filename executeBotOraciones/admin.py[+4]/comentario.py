from bs4 import BeautifulSoup
import requests
import re


def strip_tags(value):
    return re.sub(r'<[^>]*?>', ' ', value)


def devolver_comentario_evangelio():
    URL = "https://www.ciudadredonda.org/calendario-lecturas/evangelio-del-dia/comentario-homilia/hoy"

    # Realizamos la petici?n a la web
    req = requests.get(URL)

    # Comprobamos que la petici?n nos devuelve un Status Code = 200
    status_code = req.status_code
    if status_code == 200:
        # Pasamos el contenido HTML de la web a un objeto BeautifulSoup()
        html = BeautifulSoup(req.text, "html.parser")
        # site_json = json.loads(html.text)
        # print(site_json)

        # Obtenemos el texto del comentario
        table = html.find('div', attrs={'class': 'body-txt'})
        return strip_tags(str(table))

# buscarWeb()
